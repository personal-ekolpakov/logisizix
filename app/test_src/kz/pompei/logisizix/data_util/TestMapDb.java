package kz.pompei.logisizix.data_util;

import java.util.Map;
import org.mapdb.DB;
import org.mapdb.DBMaker;
import org.mapdb.HTreeMap;
import org.mapdb.Serializer;
import org.testng.annotations.Test;

public class TestMapDb {


  @Test
  public void name1() {
    try (DB db = DBMaker.fileDB("build/asd.db").make()) {
      try (HTreeMap<String, String> asd = db.hashMap("asd", Serializer.STRING, Serializer.STRING).createOrOpen()) {


        for (int i = 0; i < 1000; i++) {
          asd.put("a" + i, "value a" + i);
        }


      }
    }

  }


  @Test
  public void name2() {
    try (DB db = DBMaker.fileDB("build/asd.db").make()) {
      try (HTreeMap<String, String> asd = db.hashMap("asd", Serializer.STRING, Serializer.STRING).createOrOpen()) {


        for (final Map.Entry<String, String> e : asd.entrySet()) {
          System.out.println("3b3ePL5fjo :: " + e.getKey() + " = " + e.getValue());
        }


      }
    }

  }

  @Test
  public void name3() {
    try (DB db = DBMaker.fileDB("build/asd.db").make()) {
      try (HTreeMap<String, String> asd = db.hashMap("asd", Serializer.STRING, Serializer.STRING).createOrOpen()) {


        String a34 = asd.get("a34");
        System.out.println("m6mFVyWIhy :: a34 = " + a34);


      }
    }
  }


  @Test
  public void name4() {


    try (DB db = DBMaker.fileDB("build/asd.db").make()) {
      try (HTreeMap<String, String> asd = db.hashMap("asd", Serializer.STRING, Serializer.STRING).createOrOpen()) {



        asd.put("a34", "kjh345 543hkj jhh432 lkj43 jkl6546 654k56jlk1j4564j6");



      }
    }


  }
}
