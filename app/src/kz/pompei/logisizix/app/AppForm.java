package kz.pompei.logisizix.app;

import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import javax.swing.JFrame;

public class AppForm {
  public static void main(String[] args) {
    final AppForm appForm = new AppForm();
    appForm.showForm();
  }

  private void showForm() {
    final JFrame frame = new JFrame();

    frame.setSize(1024, 400);
    frame.setLocation(4000, 100);

    frame.setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);

    frame.addWindowListener(new WindowAdapter() {
      @Override
      public void windowClosing(WindowEvent e) {
        frame.dispose();
      }

      @Override
      public void windowClosed(WindowEvent e) {
        System.out.println("KU5x9uJym1d :: Closed");
      }
    });

    frame.setVisible(true);
  }
}
